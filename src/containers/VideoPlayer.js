import React, { Component } from 'react';
import { Player, BigPlayButton, ControlBar, PlaybackRateMenuButton } from 'video-react';

import 'video-react/dist/video-react.css';
import 'video-react/styles/scss/video-react.scss';
import 'bootstrap/dist/css/bootstrap.css'

class VideoPlayer extends Component {
    constructor(props) {
        super(props);

        this.play = this.play.bind(this);
        this.pause = this.pause.bind(this);
        this.load = this.load.bind(this);
        this.seek = this.seek.bind(this);
        this.changeCurrentTime = this.changeCurrentTime.bind(this);
    }

    componentDidMount() {
        // subscribe state change
        this.refs.player.subscribeToStateChange(this.handleStateChange.bind(this));
    }

    handleStateChange(state, prevState) {
        // copy player state to this component's state
        this.setState({
            player: state,
            currentTime: state.currentTime
        });
    }

    play() {
        this.refs.player.play();
    }

    pause() {
        this.refs.player.pause();
    }

    load() {
        this.refs.player.load();
    }

    changeCurrentTime(seconds) {
        return () => {
            const { player } = this.refs.getState();
            const currentTime = player.currentTime;
            this.refs.player.seek(currentTime + seconds);
        }
    }

    seek(seconds) {
        return () => {
            this.refs.player.seek(seconds);
        }
    }

    render() {
        return(
            <div className="container">
                <Player
                    ref="player"
                    playsInline
                    src={this.props.src}
                    poster={this.props.cover}>
                    <BigPlayButton position="center"/>
                    {this.props.sub.map((subtitle, index) => (
                        <track
                            key={index}
                            default={index === 0}
                            label={subtitle.title}
                            srcLang={subtitle.language}
                            src={subtitle.urls.stream_url}
                            kind="subtitles"/>
                    ))}
                    {/*<track>
                        label={this.props.sub[0].title}
                        srcLang={this.props.sub[0].language}
                        src={'storage/' + this.props.sub[0].storage.path}
                    </track>*/}
                    <ControlBar autoHide={false}>
                        <PlaybackRateMenuButton
                            rates={[5, 3, 1.5, 1, 0.5, 0.1]}
                            order={7.1}
                        />
                    </ControlBar>
                </Player>
            </div>)
    }
}

export default VideoPlayer